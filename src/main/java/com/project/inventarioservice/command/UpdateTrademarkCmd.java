package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.TrademarkBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Trademark;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.request.TrademarkUpdateRequest;
import com.project.inventarioservice.response.LightTrademarkResponse;
import com.project.inventarioservice.service.TrademarkService;
import com.project.inventarioservice.utils.TrademarkCompose;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateTrademarkCmd implements BusinessLogicCommand {

    private TrademarkService trademarkService;

    @Setter
    private TrademarkUpdateRequest trademarkUpdateRequest;

    @Getter
    private LightTrademarkResponse trademarkResponse;

    public UpdateTrademarkCmd(TrademarkService trademarkService) {
        this.trademarkService = trademarkService;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        update();
    }

    private boolean validate() {
        return trademarkService.exist(trademarkUpdateRequest.getTrademarkId());
    }

    private void update() {
        try {
            trademarkResponse = updateTrademark();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private LightTrademarkResponse updateTrademark() throws Exception {
        Trademark trademark = trademarkService.update(
                TrademarkCompose.composeTrademark(trademarkUpdateRequest));
        return TrademarkBuilder.lightTrademarkResponse(trademark);
    }
}
