package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteTrademarkCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeleteTrademarkController {

    private DeleteTrademarkCmd deleteTrademarkCmd;

    public DeleteTrademarkController(DeleteTrademarkCmd deleteTrademarkCmd) {
        this.deleteTrademarkCmd =  deleteTrademarkCmd;
    }

    @DeleteMapping(value = Constants.TRADEMARKS.TRADEMARKS_BY_ID_PATH)
    public ResponseEntity<Void> deleteTrademark (@RequestParam("id") Long id) {
        deleteTrademarkCmd.setTrademarkId(id);
        deleteTrademarkCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
