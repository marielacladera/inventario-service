package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Trademark;
import com.project.inventarioservice.request.TrademarkCreateRequest;
import com.project.inventarioservice.request.TrademarkUpdateRequest;

/**
 * @author Mariela Cladera M.
 */
public class TrademarkCompose {
    public static Trademark composeTrademark(TrademarkCreateRequest request) {
        Trademark trademark = new Trademark();
        trademark.setName(request.getName());
        return trademark;
    }

    public static Trademark composeTrademark(TrademarkUpdateRequest request) {
        Trademark trademark = new Trademark();
        trademark.setTrademarkId(request.getTrademarkId());
        trademark.setName(request.getName());
        return trademark;
    }
}
