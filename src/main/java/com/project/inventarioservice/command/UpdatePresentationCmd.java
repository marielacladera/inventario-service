package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.PresentationBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.request.PresentationUpdateRequest;
import com.project.inventarioservice.response.LightPresentationResponse;
import com.project.inventarioservice.service.PresentationService;
import com.project.inventarioservice.utils.PresentationCompose;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdatePresentationCmd implements BusinessLogicCommand {

    private PresentationService presentationService;

    @Setter
    private PresentationUpdateRequest presentationRequest;

    @Getter
    private LightPresentationResponse presentationResponse;

    public UpdatePresentationCmd(PresentationService presentationService) {
        this.presentationService = presentationService;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        update();
    }

    private boolean validate() {
        return presentationService.exist(presentationRequest.getPresentationId());
    }

    private void update() {
        try {
            presentationResponse = presentationUpdate();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private LightPresentationResponse presentationUpdate() throws Exception {
        Presentation instance = presentationService.
                update(PresentationCompose.composePresentation(presentationRequest));
        return PresentationBuilder.lightPresentationResponse(instance);
    }
}
