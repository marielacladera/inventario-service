package com.project.inventarioservice.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
    * @author Mariela Cladera M.
*/
@Getter
public class CustomerCreateRequest {
    @NotNull
    @Size(min = 3)
    private String name;

    @NotNull
    @Size(min = 3)
    private String lastName;

    @NotNull
    @Size(min = 3)
    private String dni;

    @NotNull
    @Size(min = 3)
    private String emailAddress;
}
