package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.FindTrademarkForIdCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.TrademarkResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class FindTrademarkForIdController {

    private FindTrademarkForIdCmd findTrademarkForIdCmd;

    public FindTrademarkForIdController(FindTrademarkForIdCmd findTrademarkForIdCmd) {
        this.findTrademarkForIdCmd = findTrademarkForIdCmd;
    }

    @GetMapping(value = Constants.TRADEMARKS.TRADEMARKS_BY_ID_PATH)
    public ResponseEntity<TrademarkResponse> listForIdTrademark(@PathVariable("id") Long id) throws Exception {
        findTrademarkForIdCmd.setTrademarkId(id);
        findTrademarkForIdCmd.execute();
        TrademarkResponse response = findTrademarkForIdCmd.getTrademarkResponse();
        if(response == null){
            throw new Exception("ID NO ENCONTRADO " + id);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
