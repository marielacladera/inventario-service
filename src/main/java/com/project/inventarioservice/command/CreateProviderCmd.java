package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.ProviderBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Provider;
import com.project.inventarioservice.request.ProviderCreateRequest;
import com.project.inventarioservice.response.ProviderResponse;
import com.project.inventarioservice.service.ProviderService;
import com.project.inventarioservice.utils.ProviderCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateProviderCmd implements BusinessLogicCommand {

    private ProviderService providerService;

    @Setter
    private ProviderCreateRequest providerCreateRequest;

    @Getter
    private ProviderResponse providerResponse;

    public CreateProviderCmd(ProviderService providerService) {
        this.providerService = providerService;
    }

    @Override
    public void execute() {
        create();
    }

    private void create() {
        try {
            providerResponse = createProvider();
        } catch (Exception e) {
            throw  new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private ProviderResponse createProvider() throws Exception {
        Provider instance = providerService.create(
                ProviderCompose.composeProvider(providerCreateRequest));
        return ProviderBuilder.providerResponse(instance);
    }
}
