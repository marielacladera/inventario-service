package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.CreateInputCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.InputResponse;
import com.project.inventarioservice.request.InputCreateRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class CreateInputController {

    private CreateInputCmd createInputCmd;

    public CreateInputController(CreateInputCmd createInputCmd) {
        this.createInputCmd = createInputCmd;
    }

    @PostMapping(value = Constants.INPUTS.INPUTS_PATH)
    public ResponseEntity<InputResponse> createInput(@Valid @RequestBody InputCreateRequest request) {
        createInputCmd.setInputCreateRequest(request);
        createInputCmd.execute();
        return new ResponseEntity<InputResponse>(createInputCmd.getInputResponse(), HttpStatus.OK);
    }
}
