package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.FindProductByIdCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.ProductResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class FindProductByIdController {

    private FindProductByIdCmd findProductByIdCmd;

    public FindProductByIdController(FindProductByIdCmd findProductByIdCmd) {
        this.findProductByIdCmd = findProductByIdCmd;
    }

    @GetMapping(value = Constants.PRODUCTS.PRODUCTS_BY_ID_PATH)
    public ResponseEntity<ProductResponse> listForIdProduct(@PathVariable("id") Long id) {
        findProductByIdCmd.setProductId(id);
        findProductByIdCmd.execute();
        return new ResponseEntity<ProductResponse>(findProductByIdCmd.getProductResponse(), HttpStatus.OK);
    }
}
