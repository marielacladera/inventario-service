package com.project.inventarioservice.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class LightProductResponse {
    private String name;

    private String description;

    private PresentationResponse presentation;

    private TrademarkResponse trademark;

    private CategoryResponse category;
}
