package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CustomerBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Customer;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.request.CustomerUpdateRequest;
import com.project.inventarioservice.response.LightCustomerResponse;
import com.project.inventarioservice.service.CustomerService;
import com.project.inventarioservice.utils.CustomerCompose;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateCustomerCmd implements BusinessLogicCommand {

    private CustomerService customerService;

    @Setter
    private CustomerUpdateRequest customerUpdateRequest;

    @Getter
    private LightCustomerResponse customerResponse;

    public UpdateCustomerCmd(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        update();
    }

    private boolean validate() {
        return customerService.exist(customerUpdateRequest.getCustomerId());
    }

    private void update() {
        try{
            customerResponse = customerUpdate();
        }catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private LightCustomerResponse customerUpdate() throws Exception {
        Customer instance = customerService
                .update(CustomerCompose.composeCustomer(customerUpdateRequest));
        return CustomerBuilder.lightCustomerResponse(instance);
    }
}
