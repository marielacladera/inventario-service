package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.OutputDetail;
import com.project.inventarioservice.response.OutputDetailResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public class OutputDetailBuilder {
    public static OutputDetailResponse outputDetailResponse(OutputDetail object) {
        OutputDetailResponse instance = new OutputDetailResponse();
        instance.setOutputDetailId(object.getOutputDetailId());
        instance.setProduct(ProductBuilder.productResponse(object.getProduct()));
        instance.setQuantity(object.getQuantity());
        instance.setUnitPrice(object.getUnitPrice());
        return  instance;
    }

    public static List<OutputDetailResponse> listOutputDetailResponse(List<OutputDetail> list) {
        List<OutputDetailResponse> responseList = new ArrayList<>();
        for (OutputDetail auxiliar : list) {
            responseList.add(outputDetailResponse(auxiliar));
        }
        return responseList;
    }
}
