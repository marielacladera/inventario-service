package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class CategoryUpdateRequest {
    @NotNull
    private Long categoryId;

    @NotNull
    @Size(min = 3)
    private String name;

    @NotNull
    @Size(min = 3)
    private String description;

    private String image;
}
