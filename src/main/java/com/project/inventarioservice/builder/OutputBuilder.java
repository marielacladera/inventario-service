package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Output;
import com.project.inventarioservice.response.OutputResponse;

/**
 * @author Mariela Cladera M.
 */
public class OutputBuilder {
    public static OutputResponse outputResponse(Output output) {
        OutputResponse response = new OutputResponse();
        response.setOutputId(output.getOutputId());
        response.setDate(output.getDate());
        response.setVoucher(output.getVoucher());
        response.setCustomer(CustomerBuilder.customerResponse(output.getCustomer()));
        response.setOutputDetail(OutputDetailBuilder.listOutputDetailResponse(output.getOutputDetail()));
        return response;
    }
}
