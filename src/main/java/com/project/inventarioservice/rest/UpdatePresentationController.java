package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.UpdatePresentationCmd;
import com.project.inventarioservice.request.PresentationUpdateRequest;
import com.project.inventarioservice.response.LightPresentationResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class UpdatePresentationController {

    private UpdatePresentationCmd updatePresentationCmd;

    public UpdatePresentationController(UpdatePresentationCmd updatePresentationCmd) {
        this.updatePresentationCmd = updatePresentationCmd;
    }

    @PutMapping(value = "/presentations")
    public ResponseEntity<LightPresentationResponse> updatePresentation(@Valid @RequestBody PresentationUpdateRequest request) {
        updatePresentationCmd.setPresentationRequest(request);
        updatePresentationCmd.execute();
        return ResponseEntity.ok(updatePresentationCmd.getPresentationResponse());
    }
}
