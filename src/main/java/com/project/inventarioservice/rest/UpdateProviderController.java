package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.UpdateProviderCmd;
import com.project.inventarioservice.request.ProviderUpdateRequest;
import com.project.inventarioservice.response.LightProviderResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class UpdateProviderController {

    private UpdateProviderCmd updateProviderCmd;

    public UpdateProviderController(UpdateProviderCmd updateProviderCmd){
        this.updateProviderCmd = updateProviderCmd;
    }

    @PutMapping(value = "/providers")
    public ResponseEntity<LightProviderResponse> updateProvider(@Valid @RequestBody ProviderUpdateRequest request) {
        updateProviderCmd.setProviderUpdateRequest(request);
        updateProviderCmd.execute();

        return new ResponseEntity<>(updateProviderCmd.getUpdateProvider(), HttpStatus.OK);
    }
}
