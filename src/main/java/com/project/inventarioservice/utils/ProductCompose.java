package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Product;
import com.project.inventarioservice.request.ProductCreateRequest;
import com.project.inventarioservice.request.ProductUpdateRequest;

/**
 * @author Mariela Cladera M.
 */
public class ProductCompose {
    public static Product composeProduct(ProductCreateRequest request) {
        Product product = new Product();
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        product.setDate(request.getDate());
        product.setPresentation(PresentationCompose.composePresentation(request.getPresentation()));
        product.setTrademark(TrademarkCompose.composeTrademark(request.getTrademark()));
        product.setCategory(CategoryCompose.composeCategory(request.getCategory()));

        return product;
    }

    public static Product composeProduct (ProductUpdateRequest request) {
        Product product = new Product();
        product.setProductId(request.getProductId());
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        product.setDate(request.getDate());
        product.setPresentation(PresentationCompose.composePresentation(request.getPresentation()));
        product.setTrademark(TrademarkCompose.composeTrademark(request.getTrademark()));
        product.setCategory(CategoryCompose.composeCategory(request.getCategory()));

        return product;
    }
}
