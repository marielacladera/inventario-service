package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Input;
import com.project.inventarioservice.request.InputCreateRequest;


/**
 * @author Mariela Cladera M.
 */
public class InputCompose {
    public static Input composeInput(InputCreateRequest request) {
        Input input = new Input();
        input.setDate(request.getDate());
        input.setProvider(ProviderCompose.composeProvider(request.getProvider()));
        input.setVoucher(request.getVoucher());
        input.setState(true);
        input.setInputDetail(InputDetailCompose.composeListInputDetail(request.getInputDetail()));
        return input;
    }
}
