package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CustomerBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Customer;
import com.project.inventarioservice.response.CustomerResponse;
import com.project.inventarioservice.service.CustomerService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListCustomerCmd implements BusinessLogicCommand {

    private CustomerService customerService;

    @Getter
    private List<CustomerResponse> customerResponse;

    public ListCustomerCmd(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public void execute() {
        list();
    }

    private void list() {
        try {
            customerResponse = customerList();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<CustomerResponse> customerList() throws Exception {
        List<CustomerResponse> list =  new ArrayList<>();
        List<Customer> instance = customerService.list();
        for (Customer customer: instance) {
            list.add(CustomerBuilder.customerResponse(customer));
        }
        return list;
    }
}
