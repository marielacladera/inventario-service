package com.project.inventarioservice.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class LightProviderResponse {
    private String name;

    private String lastName;

    private Integer phone;

    private Integer cellPhone;

    private String address;

    private String emailAddress;
}
