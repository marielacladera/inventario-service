package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CategoryBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.CategoryResponse;
import com.project.inventarioservice.service.CategoryService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindCategoryByIdCmd implements BusinessLogicCommand {
    private CategoryService categoryService;

    @Setter
    private Long id;

    @Getter
    private CategoryResponse categoryResponse;

    public FindCategoryByIdCmd(CategoryService service) {
        this.categoryService = service;
    }

    @Override
    public void execute() {
       find();
    }

    private void find() {
        try {
            categoryResponse = listForIdCategory();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    private CategoryResponse listForIdCategory() throws Exception {
        return CategoryBuilder.categoryResponse(
                categoryService.read(id));
    }
}
