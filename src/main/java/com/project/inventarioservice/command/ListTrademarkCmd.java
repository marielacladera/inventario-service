package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.TrademarkBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Trademark;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.TrademarkResponse;
import com.project.inventarioservice.service.TrademarkService;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListTrademarkCmd implements BusinessLogicCommand {

    private TrademarkService trademarkService;

    @Getter
    private List<TrademarkResponse> trademarkList;

    public ListTrademarkCmd (TrademarkService trademarkService) {
        this.trademarkService = trademarkService;
    }

    @Override
    public void execute() {
        list();
    }

    private void list() {
        try {
            trademarkList = listTrademark();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private List<TrademarkResponse>  listTrademark() throws Exception {
        List<TrademarkResponse> list = new ArrayList<>();
        List<Trademark> instance = trademarkService.list();
        for(int i = 0; i < instance.size(); i++) {
            list.add(TrademarkBuilder.trademarkResponse(instance.get(i)));
        }
        return list;
    }
}
