package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.UpdateTrademarkCmd;
import com.project.inventarioservice.request.TrademarkUpdateRequest;
import com.project.inventarioservice.response.LightTrademarkResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class UpdateTrademarkController {

    private UpdateTrademarkCmd updateTrademarkCmd;

    public UpdateTrademarkController(UpdateTrademarkCmd updateTrademarkCmd) {
        this.updateTrademarkCmd = updateTrademarkCmd;
    }

    @PutMapping(value = "/trademarks")
    public ResponseEntity<LightTrademarkResponse> updateTrademark(@RequestBody TrademarkUpdateRequest request) throws Exception {
        updateTrademarkCmd.setTrademarkUpdateRequest(request);
        updateTrademarkCmd.execute();
        return new ResponseEntity<>(updateTrademarkCmd.getTrademarkResponse(), HttpStatus.OK);
    }
}
