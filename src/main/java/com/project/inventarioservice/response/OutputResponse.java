package com.project.inventarioservice.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class OutputResponse {
    private Long outputId;

    private LocalDateTime date;

    private String voucher;

    private CustomerResponse customer;

    private List<OutputDetailResponse> outputDetail;
}
