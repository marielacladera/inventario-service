package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Input;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public interface InputRepository extends JpaRepository<Input, Long> {

    @Query(value = "SELECT * FROM input WHERE state = true AND input_Id = :inputId", nativeQuery = true)
    //@Query(value = "SELECT i FROM Input WHERE i.state = true AND i.inputId = :inputId")
    Input findInputById(@Param("inputId") Long inputId);

    @Query(value = "SELECT * FROM input WHERE state = true", nativeQuery = true)
    //@Query(value = "SELECT i FROM Input i WHERE i.state = true")
    List<Input> listInput();

    @Transactional
    @Modifying
    @Query(value= "UPDATE input SET state=false WHERE input_id = :inputId", nativeQuery = true)
    Integer desactiveInput(@Param("inputId") Long inputId);
}
