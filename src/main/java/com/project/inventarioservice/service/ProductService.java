package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Product;
import com.project.inventarioservice.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class ProductService {

    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product create(Product product) throws Exception {
        return productRepository.save(product);
    }

    public Product listForId(Long id) throws Exception {
        return productRepository.getById(id);
    }

    public List<Product> list() throws Exception {
        return productRepository.findAll();
    }

    public Product update(Product product) throws Exception{
        return  productRepository.save(product);
    }

    public void delete(Long id) throws Exception {
        productRepository.deleteById(id);
    }

    public boolean exist(Long id) {
        return productRepository.existsById(id);
    }
}
