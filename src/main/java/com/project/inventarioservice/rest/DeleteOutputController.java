package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteOutputCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeleteOutputController {

    private DeleteOutputCmd deleteOutputCmd;

    public  DeleteOutputController(DeleteOutputCmd deleteOutputCmd) {
        this.deleteOutputCmd = deleteOutputCmd;
    }

    @DeleteMapping(value = Constants.OUTPUTS.OUTPUTS_BY_ID_PATH)
    public ResponseEntity<Void> deleteOutput(@PathVariable("id") Long id) {
        deleteOutputCmd.setOutputId(id);
        deleteOutputCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
