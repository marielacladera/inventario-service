package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long customerId;

    @Column(nullable = false, length = 40)
    private String name;

    @Column(nullable = false, length = 50)
    private String lastName;

    @Column(nullable = false, length = 20)
    private String dni;

    @Column(nullable = false, length = 60)
    private String emailAddress;
}
