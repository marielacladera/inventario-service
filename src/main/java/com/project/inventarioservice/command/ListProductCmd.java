package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.PresentationBuilder;
import com.project.inventarioservice.builder.ProductBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.domain.Product;
import com.project.inventarioservice.response.PresentationResponse;
import com.project.inventarioservice.response.ProductResponse;
import com.project.inventarioservice.service.ProductService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListProductCmd implements BusinessLogicCommand {

    private ProductService productService;

    @Getter
    private List<ProductResponse> productResponse;

    public ListProductCmd(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void execute() {
        list();
    }


    private void list() {
        try{
            productResponse = listProduct();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<ProductResponse> listProduct() throws Exception {
        List<ProductResponse> list = new ArrayList<>();
        List<Product> instance = productService.list();
        for (Product product: instance) {
            list.add(ProductBuilder.productResponse(product));
        }
        return list;
    }
}
