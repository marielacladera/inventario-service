package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.CustomerService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeleteCustomerCmd implements BusinessLogicCommand {

    private CustomerService customerService;

    @Setter
    private Long customerId;

    public DeleteCustomerCmd(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public void execute() {
        if(!validate()) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        delete();
    }

    private boolean validate() {
        return customerService.exist(customerId);
    }

    private void delete() {
        try{
            customerService.delete(customerId);
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
            //throw new ModelNotFoundException("INTERNAL SERVER ERROR.");
        }
    }
}
