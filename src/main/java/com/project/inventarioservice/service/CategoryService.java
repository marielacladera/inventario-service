package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Category;
import com.project.inventarioservice.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class CategoryService {

    private CategoryRepository categoryRepository;

    public  CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category create(Category category) throws Exception {
       return categoryRepository.save(category);
    }

    public Category read(Long id) throws Exception {
        //return categoryRepository.getById(id);
        return categoryRepository.findById(id).orElse(null);
    }

    public List<Category> list() throws Exception {
        return categoryRepository.findAll();
    }

    public Category update(Category category) throws Exception {
        return categoryRepository.save(category);
    }

    public void delete(Long id) throws Exception {
        categoryRepository.deleteById(id);
    }

    public boolean exist(Long id) {
        return categoryRepository.existsById(id);
    }
}
