package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListProductCmd;
import com.project.inventarioservice.response.ProductResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListProductController {

    private ListProductCmd listProductCmd;

    public ListProductController(ListProductCmd listProductCmd){
        this.listProductCmd = listProductCmd;
    }

    @GetMapping(value = "/products")
    public ResponseEntity<List<ProductResponse>> listProduct() {
        listProductCmd.execute();
        return ResponseEntity.ok(listProductCmd.getProductResponse());
    }
}
