package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.ProviderBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.ProviderResponse;
import com.project.inventarioservice.service.ProviderService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindProviderByIdCmd implements BusinessLogicCommand {

    private ProviderService providerService;

    @Setter
    public Long id;

    @Getter
    public ProviderResponse providerResponse;

    public FindProviderByIdCmd(ProviderService providerService) {
        this.providerService = providerService;
    }

    @Override
    public void execute() {
       find();
    }

    private void find() {
        try {
            providerResponse = findProvider();
        } catch (Exception e) {
            throw  new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_404, e);
        }
    }

    private ProviderResponse findProvider() throws Exception {
        return ProviderBuilder.providerResponse(
                providerService.read(id)
        );
    }
}
