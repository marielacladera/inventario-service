package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Provider;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mariela Cladera M.
 */
public interface ProviderRepository extends JpaRepository<Provider, Long> {
}
