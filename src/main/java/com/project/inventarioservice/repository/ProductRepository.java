package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mariela Cladera M.
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}
