package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Customer;
import com.project.inventarioservice.repository.CustomerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer create(Customer customer) throws Exception {
        return customerRepository.save(customer);
    }

    public List<Customer> list() throws Exception {
        return customerRepository.findAll();
    }

    public Customer read(Long id) throws Exception {
        return customerRepository.getById(id);
    }

    public Customer update(Customer customer) throws Exception {
        return  customerRepository.save(customer);
    }

    public void delete(Long id) throws Exception {
        customerRepository.deleteById(id);
    }

    public boolean exist(Long id) {
        return customerRepository.existsById(id);
    }
}
