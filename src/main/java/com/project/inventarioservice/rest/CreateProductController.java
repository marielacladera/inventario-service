package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.CreateProductCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.ProductCreateRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class CreateProductController {

    private CreateProductCmd createProductCmd;

    public CreateProductController(CreateProductCmd createProductCmd) {
        this.createProductCmd = createProductCmd;
    }

    @PostMapping(value = Constants.PRODUCTS.PRODUCTS_PATH)
    public ResponseEntity<Void> createProduct (@Valid @RequestBody ProductCreateRequest request) {
        createProductCmd.setProductCreateRequest(request);
        createProductCmd.execute();
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(createProductCmd.getProductResponse().getProductId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
}
