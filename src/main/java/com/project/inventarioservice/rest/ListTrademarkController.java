package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListTrademarkCmd;
import com.project.inventarioservice.response.TrademarkResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListTrademarkController {

    private ListTrademarkCmd listTrademarkCmd;

    public ListTrademarkController(ListTrademarkCmd listTrademarkCmd){
        this.listTrademarkCmd = listTrademarkCmd;
    }

    @GetMapping("/trademarks")
    public ResponseEntity<List<TrademarkResponse>> listTrademarks () {
        listTrademarkCmd.execute();
        List<TrademarkResponse> response = listTrademarkCmd.getTrademarkList();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
