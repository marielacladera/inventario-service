package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Provider;
import com.project.inventarioservice.response.LightProviderResponse;
import com.project.inventarioservice.response.ProviderResponse;

/**
 * @author Mariela Cladera M.
 */
public class ProviderBuilder {
    public static ProviderResponse providerResponse(Provider provider) {
        ProviderResponse response = new ProviderResponse();
        response.setProviderId(provider.getProviderId());
        response.setName(provider.getName());
        response.setLastName(provider.getLastName());
        response.setPhone(provider.getPhone());
        response.setCellPhone(provider.getCellPhone());
        response.setAddress(provider.getAddress());
        response.setEmailAddress(provider.getEmailAddress());

        return response;
    }
    public static LightProviderResponse lightProviderResponse(Provider provider) {
        LightProviderResponse response = new LightProviderResponse();
        response.setName(provider.getName());
        response.setLastName(provider.getLastName());
        response.setPhone(provider.getPhone());
        response.setCellPhone(provider.getCellPhone());
        response.setAddress(provider.getAddress());
        response.setEmailAddress(provider.getEmailAddress());

        return response;
    }
}
