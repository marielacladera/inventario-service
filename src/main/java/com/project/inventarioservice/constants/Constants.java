package com.project.inventarioservice.constants;

/**
 * @author Mariela Cladera M.
 */
public final class Constants {

    private Constants() {

    }

    public static class ERRORS {
        public static final String ERROR_500 = "Internal service error";
        public static final String ERROR_404 = "ID not found";
    }

    public static class CATEGORIES {
        public static final String CATEGORIES_PATH = "/categories";
        public static final String CATEGORIES_BY_ID_PATH = "/categories/{id}";
    }

    public static class CUSTOMERS {
        public static final String CUSTOMERS_PATH = "/customers";
        public static final String CUSTOMERS_BY_ID_PATH = "/customers/{id}";
    }

    public static class PRESENTATIONS {
        public static final String PRESENTATIONS_PATH = "/presentations";
        public static final String PRESENTATIONS_BY_ID_PATH = "/presentations/{id}";
    }

    public static class PRODUCTS {
        public static final  String PRODUCTS_PATH = "/products";
        public static final String PRODUCTS_BY_ID_PATH = "/products/{id}";
    }

    public static class PROVIDERS {
        public static final String PROVIDERS_PATH = "/providers";
        public static final String PROVIDERS_BY_ID_PATH = "/providers/{id}";
    }

    public static class TRADEMARKS {
        public static final String TRADEMARKS_PATH = "/trademarks";
        public static final String TRADEMARKS_BY_ID_PATH = "/trademarks/{id}";
    }

    public static class INPUTS {
        public static final String INPUTS_PATH = "/inputs";
        public static final String INPUTS_BY_ID_PATH = "/inputs/{id}";
    }

    public static class OUTPUTS {
        public static final String OUTPUTS_PATH = "/outputs";
        public static final String OUTPUTS_BY_ID_PATH = "/outputs/{id}";
    }
}
