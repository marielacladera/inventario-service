package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Trademark;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.TrademarkService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeleteTrademarkCmd implements BusinessLogicCommand {

    private TrademarkService trademarkService;

    @Setter
    private Long trademarkId;

    public DeleteTrademarkCmd(TrademarkService trademarkService) {
        this.trademarkService = trademarkService;
    }

    @Override
    public void execute() {
        if(!validate()) {
            throw  new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        delete();
    }

    private boolean validate() {
        return trademarkService.exist(trademarkId);
    }

    private void delete() {
        try {
            trademarkService.delete(trademarkId);
        }catch(Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }
}
