package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Output {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long outputId;

    @Column(nullable = false)
    private LocalDateTime date;

    // numero comprobante
    @Column(nullable = false)
    private String voucher;

    @Column(nullable = false)
    private Boolean state;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false, foreignKey = @ForeignKey(name = "FK_output_customer"))
    private Customer customer;

    @OneToMany(mappedBy = "output", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<OutputDetail> outputDetail;
}
