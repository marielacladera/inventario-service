package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.UpdateProductCmd;
import com.project.inventarioservice.request.ProductUpdateRequest;
import com.project.inventarioservice.response.LightProductResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class UpdateProductController {

    private UpdateProductCmd updateProductCmd;

    public UpdateProductController(UpdateProductCmd updateProductCmd){
        this.updateProductCmd = updateProductCmd;
    }

    @PutMapping(value = "/products")
    public ResponseEntity<LightProductResponse> updateProduct(@Valid @RequestBody ProductUpdateRequest request) {
        updateProductCmd.setProductUpdateRequest(request);
        updateProductCmd.execute();
        return ResponseEntity.ok(updateProductCmd.getProductResponse());
        //return new ResponseEntity<LightProductResponse>(updateProductCmd.getProductResponse(), HttpStatus.OK);
    }
}
