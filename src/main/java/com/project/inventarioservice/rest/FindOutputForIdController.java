package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteOutputCmd;
import com.project.inventarioservice.command.FindOutputIdCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.OutputResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class FindOutputForIdController {

    private FindOutputIdCmd findOutputIdCmd;

    public FindOutputForIdController(FindOutputIdCmd findOutputIdCmd) {
        this.findOutputIdCmd = findOutputIdCmd;
    }

    @GetMapping(value = Constants.OUTPUTS.OUTPUTS_BY_ID_PATH)
    public ResponseEntity<OutputResponse> listForIdOutputs(@PathVariable("id") Long id) {
        findOutputIdCmd.setOutputId(id);
        findOutputIdCmd.execute();
        return new ResponseEntity<>(findOutputIdCmd.getResponse(), HttpStatus.OK);
    }
}
