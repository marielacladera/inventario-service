package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class OutputDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long outputDetailId;

    @ManyToOne
    @JoinColumn(name = "output_id", nullable = false, foreignKey = @ForeignKey(name = "FK_outputdetail_output"))
    private Output output;

    @OneToOne
    @JoinColumn(name = "product_id", nullable = false, foreignKey = @ForeignKey(name = "FK_outputdetail_product"))
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double unitPrice;
}
