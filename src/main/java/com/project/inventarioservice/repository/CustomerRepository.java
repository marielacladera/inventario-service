package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * @author Mariela Cladera M.
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
