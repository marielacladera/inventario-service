package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Provider;
import com.project.inventarioservice.request.ProviderCreateRequest;
import com.project.inventarioservice.request.ProviderUpdateRequest;

/**
 * @author Mariela Cladera M.
 */
public class ProviderCompose {
    public static Provider composeProvider(ProviderCreateRequest request) {
        Provider provider = new Provider();
        provider.setName(request.getName());
        provider.setLastName(request.getLastName());
        provider.setPhone(request.getPhone());
        provider.setCellPhone(request.getCellPhone());
        provider.setAddress(request.getAddress());
        provider.setEmailAddress(request.getEmailAddress());
        return provider;
    }

    public static Provider composeProvider(ProviderUpdateRequest request) {
        Provider provider = new Provider();
        provider.setProviderId(request.getProviderId());
        provider.setName(request.getName());
        provider.setLastName(request.getLastName());
        provider.setPhone(request.getPhone());
        provider.setCellPhone(request.getCellPhone());
        provider.setAddress(request.getAddress());
        provider.setEmailAddress(request.getEmailAddress());
        return provider;
    }
}
