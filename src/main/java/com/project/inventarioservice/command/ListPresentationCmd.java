package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.PresentationBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.response.PresentationResponse;
import com.project.inventarioservice.service.PresentationService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListPresentationCmd implements BusinessLogicCommand {

    private PresentationService presentationService;

    @Getter
    private List<PresentationResponse> presentationResponse;

    public ListPresentationCmd(PresentationService presentationService) {
        this.presentationService =  presentationService;
    }

    @Override
    public void execute() {
        list();
    }

    private void list() {
        try{
            presentationResponse = listPresentation();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<PresentationResponse> listPresentation() throws Exception {
        List<PresentationResponse> list = new ArrayList<>();
        List<Presentation> instance = presentationService.list();
        for (Presentation presentation: instance) {
            list.add(PresentationBuilder.presentationResponse(presentation));
        }
        return list;
    }
}
