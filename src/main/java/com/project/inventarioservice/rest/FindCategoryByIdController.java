package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.FindCategoryByIdCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.CategoryResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class FindCategoryByIdController {

    private FindCategoryByIdCmd findCategoryForIdCmd;

    public FindCategoryByIdController(FindCategoryByIdCmd findCategoryForIdCmd) {
        this.findCategoryForIdCmd = findCategoryForIdCmd;
    }

    @GetMapping(value = Constants.CATEGORIES.CATEGORIES_BY_ID_PATH)
    public ResponseEntity<CategoryResponse> listForIdCategory(@PathVariable("id") Long id) {
        findCategoryForIdCmd.setId(id);
        findCategoryForIdCmd.execute();
        return new ResponseEntity<CategoryResponse>(findCategoryForIdCmd.getCategoryResponse(), HttpStatus.OK);
        //return ResponseEntity.ok(findCategoryForIdCmd.getCategoryResponse());
    }
}
