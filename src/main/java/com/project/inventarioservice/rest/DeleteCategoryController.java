package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteCategoryCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeleteCategoryController {

    private DeleteCategoryCmd deleteCategoryCmd;

    public DeleteCategoryController(DeleteCategoryCmd deleteCategoryCmd) {
        this.deleteCategoryCmd = deleteCategoryCmd;
    }

    @DeleteMapping(value = Constants.CATEGORIES.CATEGORIES_BY_ID_PATH)
    public ResponseEntity<Void> deleteCategorie(@PathVariable("id") Long id) {
        deleteCategoryCmd.setCategoryId(id);
        deleteCategoryCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
