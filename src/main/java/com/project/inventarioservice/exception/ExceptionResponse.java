package com.project.inventarioservice.exception;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class ExceptionResponse {
    private LocalDateTime fecha;
    private String mensaje;
    private String detalles;

    public ExceptionResponse(){
    }

    public ExceptionResponse(LocalDateTime fecha, String mensaje, String detalles) {
        this.fecha = fecha;
        this.mensaje = mensaje;
        this.detalles = detalles;
    }
}
