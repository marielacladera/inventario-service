package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.CategoryService;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */

@SynchronousExecution
public class DeleteCategoryCmd implements BusinessLogicCommand {

    private CategoryService categoryService;

    @Setter
    private Long categoryId;

    public DeleteCategoryCmd(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public void execute() {
        if(!validate()) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        } else
        delete();
    }

    private boolean validate() {
        return categoryService.exist(categoryId);
    }

    private void delete() {
        try {
            categoryService.delete(categoryId);
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }
}
