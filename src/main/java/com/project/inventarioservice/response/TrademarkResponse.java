package com.project.inventarioservice.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Setter
@Getter
public class TrademarkResponse {
    private Long trademarkId;

    private String name;
}
