package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productId;

    @Column (nullable = false, length = 70)
    private String name;

    @Column (nullable = false)
    private String description;

    @Column (nullable = false)
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "presentation_id", nullable = false, foreignKey = @ForeignKey(name = "FK_product_presentation"))
    private Presentation presentation;

    @ManyToOne
    @JoinColumn(name = "trademark_id", nullable = false, foreignKey = @ForeignKey(name = "FK_product_trademark"))
    private Trademark trademark;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false, foreignKey = @ForeignKey(name = "FK_product_category"))
    private Category category;
}
