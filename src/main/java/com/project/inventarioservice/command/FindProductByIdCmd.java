package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.ProductBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.ProductResponse;
import com.project.inventarioservice.service.ProductService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindProductByIdCmd implements BusinessLogicCommand {

    private ProductService productService;

    @Setter
    private Long productId;

    @Getter
    private ProductResponse productResponse;

    public FindProductByIdCmd(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void execute() {
       find();
    }

    private void find() {
        try {
            productResponse = findProductForId();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    private ProductResponse findProductForId() throws Exception {
        return ProductBuilder.productResponse(
                productService.listForId(productId));
    }
}
