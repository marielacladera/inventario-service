package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Output;
import com.project.inventarioservice.repository.OutputRepository;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class OutputService {

    private OutputRepository outputRepository;

    public OutputService(OutputRepository outputRepository) {
        this.outputRepository = outputRepository;
    }

    public Output registerTransactional(Output output) throws Exception {
        output.getOutputDetail().forEach(det -> det.setOutput(output));
        return outputRepository.save(output);
    }

    public Output read(Long id) throws Exception {
        return outputRepository.findOutputById(id);
    }

    public List<Output> list() throws Exception {
        return outputRepository.listOutput();
    }

    public void delete(Long id) throws Exception {
       Integer row = outputRepository.desactivateOutput(id);
    }

    public boolean exist(Long id) {
        return outputRepository.existsById(id);
    }
}
