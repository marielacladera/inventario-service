package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.CreateTrademarkCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.TrademarkCreateRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class CreateTrademarkController {

    private CreateTrademarkCmd createTrademarkCmd;

    public CreateTrademarkController(CreateTrademarkCmd createTrademarkCmd) {
        this.createTrademarkCmd = createTrademarkCmd;
    }

    @PostMapping(value = Constants.TRADEMARKS.TRADEMARKS_PATH)
    public ResponseEntity<Void> createTrademark (@Valid @RequestBody TrademarkCreateRequest request) {
        createTrademarkCmd.setTrademarkCreateRequest(request);
        createTrademarkCmd.execute();
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(createTrademarkCmd.getTrademarkResponse().getTrademarkId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
}
