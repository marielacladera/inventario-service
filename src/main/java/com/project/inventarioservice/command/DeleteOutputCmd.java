package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.OutputService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeleteOutputCmd implements BusinessLogicCommand {

    private OutputService outputService;

    @Setter
    private Long outputId;

    public DeleteOutputCmd(OutputService outputService) {
        this.outputService = outputService;
    }

    @Override
    public void execute() {
        if(!validate()) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
        delete();
    }

    private boolean validate() {
        return outputService.exist(outputId);
    }

    private void delete() {
        try{
            outputService.delete(outputId);
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }
}
