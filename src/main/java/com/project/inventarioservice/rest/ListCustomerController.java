package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListCustomerCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.CustomerResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListCustomerController {

    private ListCustomerCmd listCustomerCmd;

    public ListCustomerController(ListCustomerCmd listCustomerCmd) {
        this.listCustomerCmd = listCustomerCmd;
    }

    @GetMapping(value = Constants.CUSTOMERS.CUSTOMERS_PATH)
    public ResponseEntity<List<CustomerResponse>> listCustomers () {
        listCustomerCmd.execute();
        return new ResponseEntity<List<CustomerResponse>>(listCustomerCmd.getCustomerResponse(), HttpStatus.OK);
    }
}
