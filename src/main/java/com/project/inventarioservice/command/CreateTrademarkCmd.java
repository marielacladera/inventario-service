package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.TrademarkBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Trademark;
import com.project.inventarioservice.request.TrademarkCreateRequest;
import com.project.inventarioservice.response.TrademarkResponse;
import com.project.inventarioservice.service.TrademarkService;
import com.project.inventarioservice.utils.TrademarkCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateTrademarkCmd implements BusinessLogicCommand {

    private TrademarkService trademarkService;

    @Setter
    private TrademarkCreateRequest trademarkCreateRequest;

    @Getter
    private TrademarkResponse trademarkResponse;

    public CreateTrademarkCmd(TrademarkService trademarkService) {
        this.trademarkService = trademarkService;
    }

    @Override
    public void execute() {
       create();
    }

    private void create() {
        try {
            trademarkResponse = createTrademark();
        }catch (Exception e) {
            throw  new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private TrademarkResponse createTrademark() throws Exception{
        Trademark instance = trademarkService.create(
                TrademarkCompose.composeTrademark(trademarkCreateRequest));
        return TrademarkBuilder.trademarkResponse(instance);
    }
}
