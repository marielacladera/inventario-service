package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Input {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long inputId;

    @Column(nullable = false)
    private LocalDateTime date;

    // numero comprobante
    @Column(nullable = false)
    private String voucher;

    @Column(nullable = false)
    private Boolean state;

    @ManyToOne
    @JoinColumn(name = "provider_id", nullable = false, foreignKey = @ForeignKey(name = "FK_input_provider"))
    private Provider provider;

    @OneToMany(mappedBy = "input", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<InputDetail> inputDetail;

}
