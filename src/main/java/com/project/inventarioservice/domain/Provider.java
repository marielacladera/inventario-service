package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long providerId;

    @Column(nullable = false, length = 60)
    private String name;

    @Column(nullable = false, length = 60)
    private String lastName;

    @Column(nullable = false)
    private Integer phone;

    @Column(nullable = false)
    private Integer cellPhone;

    @Column(nullable = false, length = 80)
    private String address;

    @Column(nullable = false, length = 80)
    private String emailAddress;
}
