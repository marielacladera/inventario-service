package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.CreatePresentationCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.PresentationCreateRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class CreatePresentationController {

    private CreatePresentationCmd createPresentationCmd;

    public CreatePresentationController(CreatePresentationCmd createPresentationCmd){
        this.createPresentationCmd = createPresentationCmd;
    }

    @PostMapping(value = Constants.PRESENTATIONS.PRESENTATIONS_PATH)
    public ResponseEntity<Void> createPresentation(@Valid @RequestBody PresentationCreateRequest request) {
        createPresentationCmd.setPresentationCreateRequest(request);
        createPresentationCmd.execute();
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(createPresentationCmd.getPresentationResponse().getPresentationId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
}
