package com.project.inventarioservice.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class OutputDetailCreateRequest {

    @NotNull
    private ProductUpdateRequest product;

    @NotNull
    private Integer quantity;

    @NotNull
    private Double unitPrice;
}
