package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.UpdateCategoryCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.CategoryUpdateRequest;
import com.project.inventarioservice.response.LightCategoryResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class UpdateCategoryController {

    private UpdateCategoryCmd updateCategoryCmd;

    public UpdateCategoryController(UpdateCategoryCmd updateCategoryCmd) {
        this.updateCategoryCmd = updateCategoryCmd;
    }

    @PutMapping(value = Constants.CATEGORIES.CATEGORIES_PATH)
    public ResponseEntity<LightCategoryResponse> updateCategory(@Valid @RequestBody CategoryUpdateRequest request) {
        updateCategoryCmd.setCategoryUpdateRequest(request);
        updateCategoryCmd.execute();

        return new ResponseEntity<LightCategoryResponse>(updateCategoryCmd.getCategoryResponse(), HttpStatus.OK);
    }
}
