package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.ProductService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeleteProductCmd implements BusinessLogicCommand {

    private ProductService productService;

    @Setter
    private  Long productId;

    public DeleteProductCmd(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void execute() {
        if(!validate()) {
            throw  new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        delete();
    }

    private boolean validate() {
        return productService.exist(productId);
    }

    private void delete() {
        try {
            productService.delete(productId);
        }catch(Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }
}
