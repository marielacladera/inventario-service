package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.OutputBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.OutputCreateRequest;
import com.project.inventarioservice.response.OutputResponse;
import com.project.inventarioservice.service.OutputService;
import com.project.inventarioservice.utils.OutputCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateOutputCmd implements BusinessLogicCommand {

    private OutputService outputService;

    @Setter
    private OutputCreateRequest outputCreateRequest;

    @Getter
    private OutputResponse outputResponse;

    public CreateOutputCmd(OutputService outputService) {
        this.outputService = outputService;
    }

    @Override
    public void execute() {
        create();
    }

    private void create() {
        try {
            outputResponse = createOutput();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private OutputResponse createOutput() throws Exception {
        return OutputBuilder.outputResponse(outputService.
                registerTransactional(OutputCompose.composeOutput(outputCreateRequest)));
    }
}
