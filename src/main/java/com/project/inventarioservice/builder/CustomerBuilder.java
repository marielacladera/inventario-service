package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Customer;
import com.project.inventarioservice.response.LightCustomerResponse;
import com.project.inventarioservice.response.CustomerResponse;

/**
 * @author Mariela Cladera M.
 */
public class CustomerBuilder {
    public static CustomerResponse customerResponse(Customer customer) {
        CustomerResponse response = new CustomerResponse();
        response.setCustomerId(customer.getCustomerId());
        response.setName(customer.getName());
        response.setLastName(customer.getLastName());
        response.setDni(customer.getDni());
        response.setEmailAddress(customer.getEmailAddress());

        return response;
    }

    public static LightCustomerResponse lightCustomerResponse(Customer customer){
        LightCustomerResponse response = new LightCustomerResponse();
        response.setName(customer.getName());
        response.setLastName(customer.getLastName());
        response.setDni(customer.getDni());
        response.setEmailAddress(customer.getEmailAddress());

        return response;
    }
}
