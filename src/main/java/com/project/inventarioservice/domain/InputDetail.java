package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class InputDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long inputDetailId;

    @ManyToOne
    @JoinColumn(name = "input_id", nullable = false, foreignKey = @ForeignKey(name = "FK_inputdetail_input"))
    private Input input;

    @OneToOne
    @JoinColumn(name = "product_id", nullable = false, foreignKey = @ForeignKey(name = "FK_inputdetail_product"))
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double unitPrice;
}
