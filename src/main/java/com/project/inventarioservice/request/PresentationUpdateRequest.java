package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class PresentationUpdateRequest {
    @NotNull
    private Long presentationId;

    @NotNull
    @Size(min = 3)
    private String content;
}
