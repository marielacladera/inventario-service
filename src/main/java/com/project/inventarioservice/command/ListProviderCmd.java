package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.ProviderBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Provider;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.ProviderResponse;
import com.project.inventarioservice.service.ProviderService;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListProviderCmd implements BusinessLogicCommand {

    private ProviderService providerService;

    @Getter
    private List<ProviderResponse> response;

    public ListProviderCmd(ProviderService providerService) {
        this.providerService = providerService;
    }

    @Override
    public void execute() {
        list();
    }

    private void list(){
        try {
            response = listProviders();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private List<ProviderResponse> listProviders()  throws Exception{
        List<ProviderResponse> list = new ArrayList<>();
        List<Provider> instance = providerService.list();
        for(Provider provider: instance) {
            list.add(ProviderBuilder.providerResponse(provider));
        }
        return list;
    }
}
