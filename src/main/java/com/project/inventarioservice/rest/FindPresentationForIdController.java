package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.FindPresentationByIdCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.PresentationResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class FindPresentationForIdController {

    private FindPresentationByIdCmd findPresentationForIdByCmd;

    public FindPresentationForIdController(FindPresentationByIdCmd findPresentationForIdByCmd) {
        this.findPresentationForIdByCmd = findPresentationForIdByCmd;
    }

    @GetMapping(value = Constants.PRESENTATIONS.PRESENTATIONS_BY_ID_PATH)
    public ResponseEntity<PresentationResponse> listForIdPresentation (@PathVariable("id")  Long id) {
        findPresentationForIdByCmd.setPresentationId(id);
        findPresentationForIdByCmd.execute();
        return ResponseEntity.ok(findPresentationForIdByCmd.getPresentationResponse());
    }
}
