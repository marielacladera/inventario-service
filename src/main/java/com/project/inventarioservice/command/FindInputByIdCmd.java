package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.InputBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Input;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.InputResponse;
import com.project.inventarioservice.service.InputService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindInputByIdCmd implements BusinessLogicCommand {

    private InputService inputService;

    @Setter
    private Long inputId;

    @Getter
    private InputResponse response;

    public FindInputByIdCmd(InputService inputService) {
        this.inputService = inputService;
    }

    @Override
    public void execute() {
        inputFind();
    }

    private boolean validate() {
        return inputService.exist(inputId);
    }

    public void inputFind() {
        try {
            response = find();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    public InputResponse find() throws Exception {
        Input instance = inputService.read(inputId);
        return InputBuilder.inputResponse(instance);
    }
}
