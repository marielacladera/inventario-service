package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Category;
import com.project.inventarioservice.request.CategoryCreateRequest;
import com.project.inventarioservice.request.CategoryUpdateRequest;

/**
 * @author Mariela Cladera M.
 */
public class CategoryCompose {
    public static Category composeCategory(CategoryCreateRequest request) {
        Category category = new Category();
        category.setName(request.getName());
        category.setDescription(request.getDescription());
        return category;
    }

    public static Category composeCategory(CategoryUpdateRequest request) {
        Category category = new Category();
        category.setCategoryId(request.getCategoryId());
        category.setName(request.getName());
        category.setDescription(request.getDescription());
        category.setImage(request.getImage());
        return category;
    }
}
