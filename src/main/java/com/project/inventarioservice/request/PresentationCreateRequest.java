package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class PresentationCreateRequest {
    @NotNull
    private String content;
}
