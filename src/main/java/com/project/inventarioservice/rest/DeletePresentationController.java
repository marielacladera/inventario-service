package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeletePresentationCmd;
import com.project.inventarioservice.command.DeleteProviderCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeletePresentationController {

    private DeletePresentationCmd deletePresentationCmd;

    public DeletePresentationController(DeletePresentationCmd deletePresentationCmd) {
        this.deletePresentationCmd = deletePresentationCmd;
    }

    @DeleteMapping(value = Constants.PRESENTATIONS.PRESENTATIONS_BY_ID_PATH)
    public ResponseEntity<Void> deletePresentation(@PathVariable("id") Long id) {
        deletePresentationCmd.setPresentationId(id);
        deletePresentationCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
