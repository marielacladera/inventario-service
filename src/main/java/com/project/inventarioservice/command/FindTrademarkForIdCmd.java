package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.TrademarkBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.TrademarkResponse;
import com.project.inventarioservice.service.TrademarkService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindTrademarkForIdCmd implements BusinessLogicCommand {

    private TrademarkService trademarkService;

    @Setter
    private Long trademarkId;

    @Getter
    private TrademarkResponse trademarkResponse;

    public FindTrademarkForIdCmd(TrademarkService trademarkService) {
        this.trademarkService = trademarkService;
    }

    @Override
    public void execute() {
        find();
    }

    private void find() {
        try {
            trademarkResponse = findTrademarkForId();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    private TrademarkResponse findTrademarkForId() throws Exception {
        return TrademarkBuilder.trademarkResponse
                (trademarkService.listForId(trademarkId));
    }
}
