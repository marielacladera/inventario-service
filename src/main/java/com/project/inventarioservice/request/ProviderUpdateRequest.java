package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class ProviderUpdateRequest {
    @NotNull
    private Long providerId;

    @NotNull
    @Size(min = 3)
    private String name;

    @NotNull
    @Size(min = 3)
    private String lastName;

    @NotNull
    private Integer phone;

    @NotNull
    private Integer cellPhone;

    @NotNull
    @Size(min = 3)
    private String address;

    @NotNull
    @Size(min = 3)
    private String emailAddress;
}
