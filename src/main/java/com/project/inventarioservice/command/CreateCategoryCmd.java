package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CategoryBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Category;
import com.project.inventarioservice.request.CategoryCreateRequest;
import com.project.inventarioservice.response.CategoryResponse;
import com.project.inventarioservice.service.CategoryService;
import com.project.inventarioservice.utils.CategoryCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateCategoryCmd implements BusinessLogicCommand {

    private CategoryService categoryService;

    @Setter
    private CategoryCreateRequest categoryCreateRequest;

    @Getter
    private CategoryResponse categoryResponse;

    public CreateCategoryCmd(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public void execute() {
       create();
    }

    private void create() {
        try {
            categoryResponse = createCategory();
        } catch (Exception e) {
            throw  new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private CategoryResponse createCategory() throws Exception {
        Category instance = categoryService.create(
                CategoryCompose.composeCategory(categoryCreateRequest));
        return CategoryBuilder.categoryResponse(instance);
    }
}
