package com.project.inventarioservice.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class InputDetailResponse {
    private Long inputDetailId;

    private ProductResponse product;

    private Integer quantity;

    private Double unitPrice;
}
