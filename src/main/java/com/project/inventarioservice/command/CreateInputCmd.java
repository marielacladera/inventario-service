package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.InputBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Input;
import com.project.inventarioservice.response.InputResponse;
import com.project.inventarioservice.request.InputCreateRequest;
import com.project.inventarioservice.service.InputService;
import com.project.inventarioservice.utils.InputCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateInputCmd implements BusinessLogicCommand {

    private InputService inputService;

    @Setter
    private InputCreateRequest inputCreateRequest;

    @Getter
    private InputResponse inputResponse;

    public CreateInputCmd(InputService inputService) {
        this.inputService =  inputService;
    }

    @Override
    public void execute() {
        create();
    }

    private void create() {
        try {
            inputResponse = createInput();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private InputResponse createInput() throws Exception {
        return InputBuilder.inputResponse(inputService.
                registerTransactional(InputCompose.composeInput(inputCreateRequest)));
    }
}
