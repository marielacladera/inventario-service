package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.InputDetail;
import com.project.inventarioservice.response.InputDetailResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public class InputDetailBuilder {
    public static InputDetailResponse inputDetailResponse(InputDetail object) {
        InputDetailResponse instance = new InputDetailResponse();
        instance.setInputDetailId(object.getInputDetailId());
        instance.setProduct(ProductBuilder.productResponse(object.getProduct()));
        instance.setQuantity(object.getQuantity());
        instance.setUnitPrice(object.getUnitPrice());
        return  instance;
    }

    public static List<InputDetailResponse> listInputDetailResponse(List<InputDetail> list) {
        List<InputDetailResponse> responseList = new ArrayList<>();
        for (InputDetail auxiliar : list) {
            responseList.add(inputDetailResponse(auxiliar));
        }
        return responseList;
    }
}
