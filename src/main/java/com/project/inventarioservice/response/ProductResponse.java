package com.project.inventarioservice.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class ProductResponse {
    private Long productId;

    private String name;

    private String description;

    private LocalDate date;

    private PresentationResponse presentation;

    private TrademarkResponse trademark;

    private CategoryResponse category;
}
