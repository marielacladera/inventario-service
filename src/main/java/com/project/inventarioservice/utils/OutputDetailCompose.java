package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.InputDetail;
import com.project.inventarioservice.domain.OutputDetail;
import com.project.inventarioservice.request.InputDetailCreateRequest;
import com.project.inventarioservice.request.OutputDetailCreateRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public class OutputDetailCompose {
    public static OutputDetail composeOutputDetail(OutputDetailCreateRequest request) {
        OutputDetail outputDetail = new OutputDetail();
        outputDetail.setProduct(ProductCompose.composeProduct((request.getProduct())));
        outputDetail.setQuantity(request.getQuantity());
        outputDetail.setUnitPrice(request.getUnitPrice());
        return outputDetail;
    }

    public static List<OutputDetail> composeListOutputDetail(List<OutputDetailCreateRequest> list) {
        List<OutputDetail> response = new ArrayList<>();
        for (OutputDetailCreateRequest object: list) {
            response.add(composeOutputDetail(object));
        }
        return response;
    }
}
