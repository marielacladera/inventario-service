package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.UpdateCustomerCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.CustomerUpdateRequest;
import com.project.inventarioservice.response.LightCustomerResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class UpdateCustomerController {

    private UpdateCustomerCmd updateCustomerCmd;

    public UpdateCustomerController(UpdateCustomerCmd updateCustomerCmd) {
        this.updateCustomerCmd = updateCustomerCmd;
    }

    @PutMapping(value = Constants.CUSTOMERS.CUSTOMERS_PATH)
    public ResponseEntity<LightCustomerResponse> updateCustomer(@Valid @RequestBody CustomerUpdateRequest request) {
        updateCustomerCmd.setCustomerUpdateRequest(request);
        updateCustomerCmd.execute();
        return new ResponseEntity<LightCustomerResponse>(updateCustomerCmd.getCustomerResponse(), HttpStatus.OK);
    }
}
