package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CustomerBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Customer;
import com.project.inventarioservice.request.CustomerCreateRequest;
import com.project.inventarioservice.response.CustomerResponse;
import com.project.inventarioservice.service.CustomerService;
import com.project.inventarioservice.utils.CustomerCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateCustomerCmd implements BusinessLogicCommand {

    private CustomerService customerService;

    @Setter
    private CustomerCreateRequest customerCreateRequest;

    @Getter
    private CustomerResponse customerResponse;

    public CreateCustomerCmd(CustomerService customerService){
        this.customerService = customerService;
    }

    @Override
    public void execute() {
        create();
    }

    private void create() {
        try {
            customerResponse = createCustomer();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private CustomerResponse createCustomer() throws Exception {
        Customer instance = customerService.create(
                CustomerCompose.composeCustomer(customerCreateRequest));
        return CustomerBuilder.customerResponse(instance);
    }
}
