package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.CreateCategoryCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.CategoryCreateRequest;
import com.project.inventarioservice.response.CategoryResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class CreateCategoryController {

    private CreateCategoryCmd createCategoryCmd;

    public CreateCategoryController(CreateCategoryCmd createCategoryCmd) {
        this.createCategoryCmd = createCategoryCmd;
    }

    @PostMapping(value = Constants.CATEGORIES.CATEGORIES_PATH)
    public ResponseEntity<CategoryResponse> createCategory(@Valid @RequestBody CategoryCreateRequest request) {
        createCategoryCmd.setCategoryCreateRequest(request);
        createCategoryCmd.execute();

//        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//                .buildAndExpand(createCategoryCmd.getCategoryResponse().getCategoryId())
//                .toUri();

        return ResponseEntity.ok(createCategoryCmd.getCategoryResponse());
    }
}
