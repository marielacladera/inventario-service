package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mariela Cladera M.
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
