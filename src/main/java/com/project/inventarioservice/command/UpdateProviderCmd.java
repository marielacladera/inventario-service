package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.ProviderBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Provider;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.request.ProviderUpdateRequest;
import com.project.inventarioservice.response.LightProviderResponse;
import com.project.inventarioservice.service.ProviderService;
import com.project.inventarioservice.utils.ProviderCompose;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateProviderCmd implements BusinessLogicCommand {

    private ProviderService providerService;

    @Setter
    private ProviderUpdateRequest providerUpdateRequest;

    @Getter
    private LightProviderResponse updateProvider;

    public UpdateProviderCmd(ProviderService providerService) {
        this.providerService = providerService;
    }

    @Override
    public void execute() {
        validate();
        update();
    }

    private void validate() {
        try{
            Provider provider = providerService.read(providerUpdateRequest.getProviderId());
            System.out.println(provider.getName());
        }catch (Exception e){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    private void update(){
        try {
            updateProvider = updateProvider();
        } catch (Exception e) {
            throw new ModelNotFoundException (Constants.ERRORS.ERROR_500);
        }
    }

    private LightProviderResponse updateProvider() throws Exception {
        Provider instance = providerService
                .update(ProviderCompose.composeProvider(providerUpdateRequest));
        return ProviderBuilder.lightProviderResponse(instance);
    }
}
