package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.FindCustomerByIdCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.CustomerResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class FindCustomerForIdController {

    private FindCustomerByIdCmd findCustomerByIdCmd;

    public FindCustomerForIdController(FindCustomerByIdCmd findCustomerByIdCmd) {
        this.findCustomerByIdCmd = findCustomerByIdCmd;
    }
    @GetMapping(value = Constants.CUSTOMERS.CUSTOMERS_BY_ID_PATH)
    public ResponseEntity<CustomerResponse> listForIdCustomer(@PathVariable("id") Long id) {
        findCustomerByIdCmd.setCustomerId(id);
        findCustomerByIdCmd.execute();
        return new ResponseEntity<CustomerResponse>(findCustomerByIdCmd.getCustomerResponse(), HttpStatus.OK);
    }
}
