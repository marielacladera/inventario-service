package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.CreateOutputCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.OutputCreateRequest;
import com.project.inventarioservice.response.OutputResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class CreateOutputController {

    private CreateOutputCmd createOutputCmd;

    public CreateOutputController(CreateOutputCmd createOutputCmd) {
        this.createOutputCmd = createOutputCmd;
    }

    @PostMapping(value = Constants.OUTPUTS.OUTPUTS_PATH)
    public ResponseEntity<OutputResponse> createOutput(@Valid @RequestBody OutputCreateRequest request) {
        createOutputCmd.setOutputCreateRequest(request);
        createOutputCmd.execute();
        return ResponseEntity.ok(createOutputCmd.getOutputResponse());
    }
}
