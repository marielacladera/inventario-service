package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Trademark;
import com.project.inventarioservice.response.LightTrademarkResponse;
import com.project.inventarioservice.response.TrademarkResponse;

/**
 * @author Mariela Cladera M.
 */
public class TrademarkBuilder {
    public static TrademarkResponse trademarkResponse(Trademark trademark) {
        TrademarkResponse response = new TrademarkResponse();
        response.setTrademarkId(trademark.getTrademarkId());
        response.setName(trademark.getName());
        return response;
    }

    public static LightTrademarkResponse lightTrademarkResponse(Trademark trademark) {
        LightTrademarkResponse response = new LightTrademarkResponse();
        response.setName(trademark.getName());

        return response;
    }
}
