package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class TrademarkUpdateRequest {
    @NotNull
    private Long trademarkId;

    @NotNull
    @Size(min = 3)
    private String name;
}
