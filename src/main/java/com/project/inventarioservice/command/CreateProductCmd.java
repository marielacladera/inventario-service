package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.ProductBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Product;
import com.project.inventarioservice.request.ProductCreateRequest;
import com.project.inventarioservice.response.ProductResponse;
import com.project.inventarioservice.service.ProductService;
import com.project.inventarioservice.utils.ProductCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreateProductCmd implements BusinessLogicCommand {

    private ProductService productService;

    @Setter
    private ProductCreateRequest productCreateRequest;

    @Getter
    private ProductResponse productResponse;

    public CreateProductCmd(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void execute() {
        create();
    }

    private void create() {
        try {
            productResponse = createProduct();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private ProductResponse createProduct() throws Exception{
        Product instance = productService.
                create(ProductCompose.composeProduct(productCreateRequest));
        return ProductBuilder.productResponse(instance);
    }
}
