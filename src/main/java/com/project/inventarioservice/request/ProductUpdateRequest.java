package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class ProductUpdateRequest {
    @NotNull
    private Long productId;

    @NotNull
    @Size(min = 3)
    private String name;

    @NotNull
    @Size(min = 3)
    private String description;

    @NotNull
    private LocalDate date;

    @NotNull
    private PresentationUpdateRequest presentation;

    @NotNull
    private TrademarkUpdateRequest trademark;

    @NotNull
    private CategoryUpdateRequest category;
}
