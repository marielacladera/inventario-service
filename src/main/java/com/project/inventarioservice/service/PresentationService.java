package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.repository.PresentationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class PresentationService {

    private PresentationRepository presentationRepository;

    public PresentationService(PresentationRepository presentationRepository) {
        this.presentationRepository = presentationRepository;
    }

    public Presentation create(Presentation presentation) throws Exception {
       return presentationRepository.save(presentation);
    }

    public List<Presentation> list() throws Exception {
        return presentationRepository.findAll();
    }

    public Presentation read(Long id) throws Exception {
        return presentationRepository.getById(id);
    }

    public Presentation update(Presentation presentation) throws Exception {
        return  presentationRepository.save(presentation);
    }

    public void delete(Long id) throws Exception {
        presentationRepository.deleteById(id);
    }

    public boolean exist(Long id) {
        return presentationRepository.existsById(id);
    }
}
