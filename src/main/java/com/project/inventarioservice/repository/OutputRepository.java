package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Output;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public interface OutputRepository extends JpaRepository<Output, Long> {

    @Query(value = "SELECT * FROM output WHERE state = true AND output_id = :outputId", nativeQuery = true)
    Output findOutputById(@Param("outputId") Long outputId);

    @Query(value = "SELECT * FROM output WHERE state = true", nativeQuery = true)
    List<Output> listOutput();

    @Transactional
    @Modifying
    @Query(value= "UPDATE output SET state=false WHERE output_id = :outputId", nativeQuery = true)
    Integer desactivateOutput(@Param("outputId") Long outputId);
}