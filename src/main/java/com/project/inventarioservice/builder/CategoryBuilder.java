package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Category;
import com.project.inventarioservice.response.CategoryResponse;
import com.project.inventarioservice.response.LightCategoryResponse;

/**
 * @author Mariela Cladera M.
 */
public class CategoryBuilder {
    public static CategoryResponse categoryResponse(Category category) {
        CategoryResponse response = new CategoryResponse();
        response.setCategoryId(category.getCategoryId());
        response.setName(category.getName());
        response.setDescription(category.getDescription());
        response.setImage(category.getImage());

        return response;
    }

    public static LightCategoryResponse lightCategoryResponse(Category category) {
        LightCategoryResponse response = new LightCategoryResponse();
        response.setName(category.getName());
        response.setDescription(category.getDescription());

        return response;
    }
}
