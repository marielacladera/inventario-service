package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.PresentationBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.PresentationResponse;
import com.project.inventarioservice.service.PresentationService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindPresentationByIdCmd implements BusinessLogicCommand {

    private PresentationService presentationService;

    @Setter
    private Long presentationId;

    @Getter
    private PresentationResponse presentationResponse;

    public FindPresentationByIdCmd(PresentationService presentationService) {
        this.presentationService = presentationService;
    }

    @Override
    public void execute() {
        find();
    }

    private void find() {
        try {
            presentationResponse = presentationFind();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    private PresentationResponse presentationFind() throws Exception {
        Presentation instance = presentationService.read(presentationId);
        return PresentationBuilder.presentationResponse(instance);
    }
}
