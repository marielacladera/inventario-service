package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.CreateProviderCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.request.ProviderCreateRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class CreateProviderController {

    private CreateProviderCmd createProviderCmd;

    public CreateProviderController(CreateProviderCmd createProviderCmd) {
        this.createProviderCmd = createProviderCmd;
    }

    @PostMapping(value = Constants.PROVIDERS.PROVIDERS_PATH)
    public ResponseEntity<Void> createProvider (@Valid @RequestBody ProviderCreateRequest request) {
        createProviderCmd.setProviderCreateRequest(request);
        createProviderCmd.execute();
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(createProviderCmd.getProviderResponse().getProviderId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
}
