package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteInputCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeleteInputController {

    private DeleteInputCmd deleteInputCmd;

    public DeleteInputController(DeleteInputCmd deleteInputCmd) {
        this.deleteInputCmd = deleteInputCmd;
    }

    @DeleteMapping(value = Constants.INPUTS.INPUTS_BY_ID_PATH)
    public ResponseEntity<Void> deleteInput(@PathVariable("id") Long id) {
        deleteInputCmd.setInputId(id);
        deleteInputCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

