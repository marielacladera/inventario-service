package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.InputService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeleteInputCmd implements BusinessLogicCommand {

    private InputService inputService;

    @Setter
    private Long inputId;

    public DeleteInputCmd(InputService inputService) {
        this.inputService = inputService;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        delete();
    }

    private boolean validate() {
        return inputService.exist(inputId);
    }

    private void delete() {
        try {
            inputService.delete(inputId);
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }
}
