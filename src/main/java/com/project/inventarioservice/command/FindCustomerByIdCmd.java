package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CustomerBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.CustomerResponse;
import com.project.inventarioservice.service.CustomerService;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindCustomerByIdCmd implements BusinessLogicCommand {

    private CustomerService customerService;

    @Setter
    private Long customerId;

    @Getter
    private CustomerResponse customerResponse;

    public FindCustomerByIdCmd(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public void execute() {
        find();
    }

    private void find() {
        try {
            customerResponse = customerFind();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    private CustomerResponse customerFind() throws Exception {
        return CustomerBuilder.customerResponse(
                customerService.read(customerId));
    }
}
