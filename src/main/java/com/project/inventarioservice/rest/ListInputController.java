package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListInputCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.InputResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListInputController {

    private ListInputCmd listInputCmd;

    public ListInputController(ListInputCmd listInputCmd) {
        this.listInputCmd = listInputCmd;
    }

    @GetMapping(value = Constants.INPUTS.INPUTS_PATH)
    public ResponseEntity<List<InputResponse>> listInputs() {
        listInputCmd.execute();
        return new ResponseEntity<>(listInputCmd.getOutputResponse(), HttpStatus.OK);
    }
}
