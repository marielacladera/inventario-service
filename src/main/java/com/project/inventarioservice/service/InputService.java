package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Input;
import com.project.inventarioservice.repository.InputRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class InputService {

    private InputRepository inputRepository;

    public InputService(InputRepository inputRepository) {
        this.inputRepository = inputRepository;
    }

    public Input registerTransactional(Input input) throws Exception {
        input.getInputDetail().forEach(det -> det.setInput(input));
        return inputRepository.save(input);
    }

    public Input read(Long id) throws Exception {
        return inputRepository.findInputById(id);
    }

    public List<Input> list() throws Exception {
        return inputRepository.listInput();
    }

    public void delete(Long id) throws Exception {
        Integer row = inputRepository.desactiveInput(id);
    }

    public boolean exist(Long id) {
        return inputRepository.existsById(id);
    }
}
