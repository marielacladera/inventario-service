package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteProductCmd;
import com.project.inventarioservice.command.DeleteProviderCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeleteProductController {

    DeleteProductCmd deleteProductCmd;

    public DeleteProductController(DeleteProductCmd deleteProductCmd) {
        this.deleteProductCmd = deleteProductCmd;
    }

    @DeleteMapping(value = Constants.PRODUCTS.PRODUCTS_BY_ID_PATH)
    public ResponseEntity<Void> deleteProvider(@PathVariable("id") Long id) {
        deleteProductCmd.setProductId(id);
        deleteProductCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
