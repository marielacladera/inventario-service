package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.response.LightPresentationResponse;
import com.project.inventarioservice.response.PresentationResponse;

/**
 * @author Mariela Cladera M.
 */
public class PresentationBuilder {
    public static PresentationResponse presentationResponse(Presentation presentation) {
        PresentationResponse response = new PresentationResponse();
        response.setPresentationId(presentation.getPresentationId());
        response.setContent(presentation.getContent());

        return response;
    }

    public static LightPresentationResponse lightPresentationResponse(Presentation presentation) {
        LightPresentationResponse response = new LightPresentationResponse();
        response.setContent(presentation.getContent());

        return response;
    }
}
