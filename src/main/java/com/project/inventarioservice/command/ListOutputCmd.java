package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.OutputBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Output;
import com.project.inventarioservice.response.OutputResponse;
import com.project.inventarioservice.service.OutputService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListOutputCmd implements BusinessLogicCommand {

    private OutputService outputService;

    @Getter
    private List<OutputResponse> inputResponse;

    public ListOutputCmd(OutputService outputService) {
        this.outputService = outputService;
    }

    @Override
    public void execute() {
        outputList();
    }

    private void outputList() {
        try {
            inputResponse = list();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.OK, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<OutputResponse> list() throws Exception {
        List<OutputResponse> auxiliarList = new ArrayList<>();
        List<Output> instance = outputService.list();
        for (Output output: instance) {
            auxiliarList.add(OutputBuilder.outputResponse(output));
        }
        return list();
    }
}
