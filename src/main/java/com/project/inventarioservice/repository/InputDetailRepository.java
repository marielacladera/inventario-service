package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.InputDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mariela Cladera M.
 */
public interface InputDetailRepository extends JpaRepository<InputDetail, Long> {
}
