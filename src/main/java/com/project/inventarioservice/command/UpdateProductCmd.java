package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.ProductBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Product;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.request.ProductUpdateRequest;
import com.project.inventarioservice.response.LightProductResponse;
import com.project.inventarioservice.service.ProductService;
import com.project.inventarioservice.utils.ProductCompose;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateProductCmd implements BusinessLogicCommand {

    private ProductService productService;

    @Setter
    private ProductUpdateRequest productUpdateRequest;

    @Getter
    private LightProductResponse productResponse;

    public UpdateProductCmd(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        update();
    }

    private boolean validate() {
        return productService.exist(productUpdateRequest.getProductId());
    }

    private void update() {
        try {
            productResponse = productUpdate();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private LightProductResponse productUpdate() throws Exception {
        Product instance = productService.
                update(ProductCompose.composeProduct(productUpdateRequest));
        return ProductBuilder.lightProductResponse(instance);
    }
}
