package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.OutputBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Output;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.OutputResponse;
import com.project.inventarioservice.service.OutputService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class FindOutputIdCmd implements BusinessLogicCommand {

    private OutputService outputService;

    @Setter
    private Long outputId;

    @Getter
    private OutputResponse response;

    public FindOutputIdCmd(OutputService outputService) {
        this.outputService = outputService;
    }

    @Override
    public void execute() {
        ouputFindById();
    }

    public void ouputFindById() {
        try {
            response = find();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
    }

    public OutputResponse find() throws Exception {
        Output instance = outputService.read(outputId);
        return OutputBuilder.outputResponse(instance);
    }
}
