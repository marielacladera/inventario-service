package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.OutputDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mariela Cladera M.
 */
public interface OutputDetailRepository extends JpaRepository<OutputDetail, Long> {
}
