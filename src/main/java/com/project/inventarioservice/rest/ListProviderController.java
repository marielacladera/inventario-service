package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListProviderCmd;
import com.project.inventarioservice.response.ProviderResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListProviderController {

    private ListProviderCmd listProviderCmd;

    public ListProviderController(ListProviderCmd listProviderCmd) {
        this.listProviderCmd = listProviderCmd;
    }

    @GetMapping(value = "/providers")
    public ResponseEntity<List<ProviderResponse>> listProviders () {
        listProviderCmd.execute();
        List<ProviderResponse> response = listProviderCmd.getResponse();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
