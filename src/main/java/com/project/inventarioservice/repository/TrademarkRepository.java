package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Trademark;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mariela Cladera M.
 */
public interface TrademarkRepository extends JpaRepository<Trademark, Long> {
}
