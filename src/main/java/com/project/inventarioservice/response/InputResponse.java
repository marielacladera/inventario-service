package com.project.inventarioservice.response;

import com.project.inventarioservice.domain.InputDetail;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class InputResponse {
    private Long inputId;

    private LocalDateTime date;

    private String voucher;

    private ProviderResponse provider;

    private List<InputDetailResponse> inputDetail;
}
