package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Provider;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.ProviderService;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeleteProviderCmd implements BusinessLogicCommand {

    private ProviderService providerService;

    @Setter
    private Long providerId;

    public DeleteProviderCmd(ProviderService providerService) {
        this.providerService = providerService;
    }

    @Override
    public void execute() {
        if(!validate()) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        delete();
    }

    private boolean validate() {
        return providerService.exist(providerId);
    }

    private void delete() {
        try{
            providerService.delete(providerId);
        }catch (Exception e){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }
}
