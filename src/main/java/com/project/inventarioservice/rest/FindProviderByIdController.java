package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.FindProviderByIdCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.ProviderResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class FindProviderByIdController {

    private FindProviderByIdCmd findProviderByIdCmd;
    public FindProviderByIdController(FindProviderByIdCmd findProviderByIdCmd) {
        this.findProviderByIdCmd = findProviderByIdCmd;
    }

    @GetMapping(value = Constants.PROVIDERS.PROVIDERS_BY_ID_PATH)
    public ResponseEntity<ProviderResponse> listForIdProvider (@PathVariable("id") Long id) {
        findProviderByIdCmd.setId(id);
        findProviderByIdCmd.execute();
        return new ResponseEntity<ProviderResponse>(findProviderByIdCmd.getProviderResponse(), HttpStatus.OK);
    }
}
