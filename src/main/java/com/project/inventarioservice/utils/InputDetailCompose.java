package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.InputDetail;
import com.project.inventarioservice.request.InputDetailCreateRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
public class InputDetailCompose {
    public static InputDetail composeInputDetail(InputDetailCreateRequest request) {
        InputDetail inputDetail = new InputDetail();
        inputDetail.setProduct(ProductCompose.composeProduct((request.getProduct())));
        inputDetail.setQuantity(request.getQuantity());
        inputDetail.setUnitPrice(request.getUnitPrice());
        return inputDetail;
    }

    public static List<InputDetail> composeListInputDetail(List<InputDetailCreateRequest> list) {
        List<InputDetail> response = new ArrayList<>();
        for (InputDetailCreateRequest object: list) {
            response.add(composeInputDetail(object));
        }
        return response;
    }
}
