package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CategoryBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Category;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.request.CategoryUpdateRequest;
import com.project.inventarioservice.response.LightCategoryResponse;
import com.project.inventarioservice.service.CategoryService;
import com.project.inventarioservice.utils.CategoryCompose;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class UpdateCategoryCmd implements BusinessLogicCommand {

    private CategoryService categoryService;

    @Setter
    private CategoryUpdateRequest categoryUpdateRequest;

    @Getter
    private LightCategoryResponse categoryResponse;

    public UpdateCategoryCmd(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public void execute() {
       if(!validate()){
           throw new ModelNotFoundException(Constants.ERRORS.ERROR_404);
       }

       update();
        /*if(validate()){
            update();
        }*/
    }

    private boolean validate(){
        return categoryService.exist(categoryUpdateRequest.getCategoryId());
    }

    /*private boolean validate(){
        if(find() == null){
            throw new ModelNotFoundException("ID: " + categoryUpdateRequest.getCategoryId() + " not found");
        } else return true;
    }

    private Category find() {
        try {
            return categoryService.read(categoryUpdateRequest.getCategoryId());
        } catch (Exception e){
            throw new ModelNotFoundException("Internal error");
        }
    }*/

    private void update() {
        try {
            categoryResponse = updateCategory();
        } catch (Exception e){
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private LightCategoryResponse updateCategory() throws Exception {
        Category instance = categoryService
                .update(CategoryCompose.composeCategory(categoryUpdateRequest));
        return CategoryBuilder.lightCategoryResponse(instance);
    }
}
