package com.project.inventarioservice.repository;

import com.project.inventarioservice.domain.Presentation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mariela Cladera M.
 */
public interface PresentationRepository extends JpaRepository<Presentation, Long> {
}
