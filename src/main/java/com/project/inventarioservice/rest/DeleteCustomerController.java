package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteCustomerCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeleteCustomerController {
    private DeleteCustomerCmd deleteCustomerCmd;
    public DeleteCustomerController(DeleteCustomerCmd deleteCustomerCmd) {
        this.deleteCustomerCmd = deleteCustomerCmd;
    }

    @DeleteMapping(value = Constants.CUSTOMERS.CUSTOMERS_BY_ID_PATH)
    public ResponseEntity<Void> deleteCustomers (@PathVariable("id") Long id) {
        deleteCustomerCmd.setCustomerId(id);
        deleteCustomerCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
