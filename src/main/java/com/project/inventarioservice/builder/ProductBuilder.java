package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Product;
import com.project.inventarioservice.response.LightProductResponse;
import com.project.inventarioservice.response.ProductResponse;

/**
 * @author Mariela Cladera M.
 */
public class ProductBuilder {
    public static ProductResponse productResponse(Product product) {
        ProductResponse response = new ProductResponse();
        response.setProductId(product.getProductId());
        response.setName(product.getName());
        response.setDescription(product.getDescription());
        response.setDate(product.getDate());
        response.setPresentation(PresentationBuilder.presentationResponse(product.getPresentation()));
        response.setTrademark(TrademarkBuilder.trademarkResponse(product.getTrademark()));
        response.setCategory(CategoryBuilder.categoryResponse(product.getCategory()));

        return response;
    }

    public static LightProductResponse lightProductResponse(Product product) {
        LightProductResponse response = new LightProductResponse();
        response.setName(product.getName());
        response.setDescription(product.getDescription());
        response.setPresentation(PresentationBuilder.presentationResponse(product.getPresentation()));
        response.setTrademark(TrademarkBuilder.trademarkResponse(product.getTrademark()));
        response.setCategory(CategoryBuilder.categoryResponse(product.getCategory()));

        return response;
    }
}
