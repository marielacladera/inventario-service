package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Customer;
import com.project.inventarioservice.request.CustomerCreateRequest;
import com.project.inventarioservice.request.CustomerUpdateRequest;

/**
 * @author Mariela Cladera M.
 */
public class CustomerCompose {
    public static Customer composeCustomer(CustomerCreateRequest request) {
        Customer customer = new Customer();
        customer.setName(request.getName());
        customer.setLastName(request.getLastName());
        customer.setDni(request.getDni());
        customer.setEmailAddress(request.getEmailAddress());
        return customer;
    }

    public static Customer composeCustomer(CustomerUpdateRequest request) {
        Customer customer = new Customer();
        customer.setCustomerId(request.getCustomerId());
        customer.setName(request.getName());
        customer.setLastName(request.getLastName());
        customer.setDni(request.getDni());
        customer.setEmailAddress(request.getEmailAddress());
        return customer;
    }
}
