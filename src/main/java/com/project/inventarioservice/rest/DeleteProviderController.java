package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.DeleteProviderCmd;
import com.project.inventarioservice.constants.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class DeleteProviderController {

    DeleteProviderCmd deleteProviderCmd;

    public DeleteProviderController(DeleteProviderCmd deleteProviderCmd) {
        this.deleteProviderCmd = deleteProviderCmd;
    }

    @DeleteMapping(value = Constants.PROVIDERS.PROVIDERS_BY_ID_PATH)
    public ResponseEntity<Void> deleteProvider(@PathVariable("id") Long id) {
        deleteProviderCmd.setProviderId(id);
        deleteProviderCmd.execute();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
