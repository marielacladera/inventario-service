package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.service.PresentationService;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class DeletePresentationCmd implements BusinessLogicCommand {

    private PresentationService presentationService;

    @Setter
    private Long presentationId;

    public DeletePresentationCmd(PresentationService presentationService) {
        this.presentationService = presentationService;
    }

    @Override
    public void execute() {
        if(!validate()){
            throw  new ModelNotFoundException(Constants.ERRORS.ERROR_404);
        }
        delete();
    }

    private boolean validate() {
        return presentationService.exist(presentationId);
    }

    private void delete() {
        try {
            presentationService.delete(presentationId);
        }catch(Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }
}
