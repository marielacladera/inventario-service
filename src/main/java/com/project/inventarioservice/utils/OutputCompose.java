package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Output;
import com.project.inventarioservice.request.OutputCreateRequest;

/**
 * @author Mariela Cladera M.
 */
public class OutputCompose {
    public static Output composeOutput(OutputCreateRequest request) {
        Output input = new Output();
        input.setDate(request.getDate());
        input.setCustomer(CustomerCompose.composeCustomer(request.getCustomer()));
        input.setVoucher(request.getVoucher());
        input.setState(true);
        input.setOutputDetail(OutputDetailCompose.composeListOutputDetail(request.getOutputDetail()));
        return input;
    }
}
