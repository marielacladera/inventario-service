package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class InputCreateRequest {
    @NotNull
    private LocalDateTime date;

    @NotNull
    private String voucher;

    @NotNull
    private ProviderUpdateRequest provider;

    @NotNull
    private List<InputDetailCreateRequest> inputDetail;
}
