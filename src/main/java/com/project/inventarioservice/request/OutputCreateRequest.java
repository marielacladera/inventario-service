package com.project.inventarioservice.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Getter
public class OutputCreateRequest {
    @NotNull
    private LocalDateTime date;

    @NotNull
    private String voucher;

    @NotNull
    private CustomerUpdateRequest customer;

    @NotNull
    private List<OutputDetailCreateRequest> outputDetail;
}
