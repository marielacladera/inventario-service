package com.project.inventarioservice.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
public class LightCustomerResponse {
    private String name;

    private String lastName;

    private String dni;

    private String emailAddress;
}
