package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Trademark;
import com.project.inventarioservice.repository.TrademarkRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class TrademarkService {

    private TrademarkRepository trademarkRepository;

    public TrademarkService(TrademarkRepository trademarkRepository) {
        this.trademarkRepository = trademarkRepository;
    }

    public Trademark create(Trademark trademark) throws Exception {
        return trademarkRepository.save(trademark);
    }

    public List<Trademark> list() throws Exception {
        return trademarkRepository.findAll();
    }

    public Trademark listForId(Long id) throws Exception {
        return trademarkRepository.getById(id);
    }

    public Trademark update(Trademark trademark) throws Exception {
        return  trademarkRepository.save(trademark);
    }

    public void delete(Long id) throws Exception {
        trademarkRepository.deleteById(id);
    }

    public boolean exist(Long id) {
        return trademarkRepository.existsById(id);
    }
}
