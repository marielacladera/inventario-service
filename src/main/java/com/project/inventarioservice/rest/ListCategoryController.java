package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListCategoryCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.CategoryResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListCategoryController {

    private ListCategoryCmd listCategoryCmd;

    public ListCategoryController(ListCategoryCmd listCategoryCmd) {
        this.listCategoryCmd = listCategoryCmd;
    }

    @GetMapping(value = Constants.CATEGORIES.CATEGORIES_PATH)
    public ResponseEntity<List<CategoryResponse>> listCategories(){
        listCategoryCmd.execute();
        return new ResponseEntity<>(listCategoryCmd.getResponse(), HttpStatus.OK);
    }
}
