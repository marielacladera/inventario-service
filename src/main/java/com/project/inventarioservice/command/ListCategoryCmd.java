package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.CategoryBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Category;
import com.project.inventarioservice.exception.ModelNotFoundException;
import com.project.inventarioservice.response.CategoryResponse;
import com.project.inventarioservice.service.CategoryService;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListCategoryCmd implements BusinessLogicCommand {

    private CategoryService categoryService;

    @Getter
    private List<CategoryResponse> response;

    public ListCategoryCmd(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public void execute() {
        list();
    }

    private void list() {
        try {
            response = listCategory();
        } catch (Exception e) {
            throw new ModelNotFoundException(Constants.ERRORS.ERROR_500);
        }
    }

    private List<CategoryResponse> listCategory() throws Exception {
        List<CategoryResponse> list = new ArrayList<>();
        List<Category> instance = categoryService.list();
        for (Category category: instance) {
            list.add(CategoryBuilder.categoryResponse(category));
        }
        return list;
    }
}
