package com.project.inventarioservice.service;

import com.project.inventarioservice.domain.Provider;
import com.project.inventarioservice.repository.ProviderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@Service
public class ProviderService {

    private ProviderRepository providerRepository;

    public ProviderService(ProviderRepository providerRepository) {
        this.providerRepository = providerRepository;
    }

    public Provider create(Provider provider) throws Exception{
        return providerRepository.save(provider);
    }

    public Provider read(Long id) throws Exception {
        return providerRepository.getById(id);
    }

    public List<Provider> list() throws Exception {
        return providerRepository.findAll();
    }

    public Provider update(Provider provider) throws Exception {
        return providerRepository.save(provider);
    }

    public void delete(Long id) throws Exception {
        providerRepository.deleteById(id);
    }

    public boolean exist(Long id) {
        return providerRepository.existsById(id);
    }
}
