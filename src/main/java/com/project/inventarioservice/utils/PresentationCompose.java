package com.project.inventarioservice.utils;

import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.request.PresentationCreateRequest;
import com.project.inventarioservice.request.PresentationUpdateRequest;

/**
 * @author Mariela Cladera M.
 */
public class PresentationCompose {
    public static Presentation composePresentation(PresentationCreateRequest request) {
        Presentation presentation = new Presentation();
        presentation.setContent(request.getContent());
        return presentation;
    }

    public static Presentation composePresentation (PresentationUpdateRequest request) {
        Presentation presentation = new Presentation();
        presentation.setPresentationId(request.getPresentationId());
        presentation.setContent(request.getContent());
        return presentation;
    }
}
