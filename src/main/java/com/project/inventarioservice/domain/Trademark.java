package com.project.inventarioservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Mariela Cladera M.
 */
@Getter
@Setter
@Entity
public class Trademark {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long trademarkId;

    @Column(nullable = false, length = 50)
    private String name;
}
