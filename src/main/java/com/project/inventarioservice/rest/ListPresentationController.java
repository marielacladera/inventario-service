package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListPresentationCmd;
import com.project.inventarioservice.response.PresentationResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListPresentationController {

    private ListPresentationCmd listPresentationCmd;

    public ListPresentationController(ListPresentationCmd listPresentationCmd) {
        this.listPresentationCmd = listPresentationCmd;
    }

    @GetMapping(value = "/presentations")
    public ResponseEntity<List<PresentationResponse>> listPresentation(){
        listPresentationCmd.execute();
        return new ResponseEntity<>(listPresentationCmd.getPresentationResponse(), HttpStatus.OK);
    }
}
