package com.project.inventarioservice.builder;

import com.project.inventarioservice.domain.Input;
import com.project.inventarioservice.response.InputResponse;

/**
 * @author Mariela Cladera M.
 */
public class InputBuilder {
    public static InputResponse inputResponse(Input input) {
        InputResponse response = new InputResponse();
        response.setInputId(input.getInputId());
        response.setDate(input.getDate());
        response.setVoucher(input.getVoucher());
        response.setProvider(ProviderBuilder.providerResponse(input.getProvider()));
        response.setInputDetail(InputDetailBuilder.listInputDetailResponse(input.getInputDetail()));
        return response;
    }
}
