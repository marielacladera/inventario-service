package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.InputBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Input;
import com.project.inventarioservice.response.InputResponse;
import com.project.inventarioservice.service.InputService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class ListInputCmd implements BusinessLogicCommand {
    private InputService inputService;

    @Getter
    public List<InputResponse> outputResponse;

    public ListInputCmd(InputService inputService) {
        this.inputService = inputService;
    }

    @Override
    public void execute() {
        inputList();
    }

    private void inputList() {
        try{
            outputResponse = list();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.OK, Constants.ERRORS.ERROR_500, e);
        }
    }

    private List<InputResponse> list() throws Exception {
        List<InputResponse> auxiliarList = new ArrayList<>();
        List<Input> instance = inputService.list();
        instance.forEach(input -> auxiliarList.add(InputBuilder.inputResponse(input)));
        /*for (Input input: instance) {
            auxiliarList.add(InputBuilder.inputResponse(input));
        }*/
        return list();
    }
}
