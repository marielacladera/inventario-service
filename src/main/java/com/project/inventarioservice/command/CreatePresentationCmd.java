package com.project.inventarioservice.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.project.inventarioservice.builder.PresentationBuilder;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.domain.Presentation;
import com.project.inventarioservice.request.PresentationCreateRequest;
import com.project.inventarioservice.response.PresentationResponse;
import com.project.inventarioservice.service.PresentationService;
import com.project.inventarioservice.utils.PresentationCompose;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Mariela Cladera M.
 */
@SynchronousExecution
public class CreatePresentationCmd implements BusinessLogicCommand {

    private PresentationService presentationService;

    @Setter
    private PresentationCreateRequest presentationCreateRequest;

    @Getter
    private PresentationResponse presentationResponse;

    public CreatePresentationCmd(PresentationService presentationService) {
        this.presentationService = presentationService;
    }

    @Override
    public void execute() {
        create();
    }

    private void create() {
        try {
            presentationResponse = createPresentation();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constants.ERRORS.ERROR_500, e);
        }
    }

    private PresentationResponse createPresentation() throws Exception {
        Presentation instance = presentationService.create(
                PresentationCompose.composePresentation(presentationCreateRequest));
        return PresentationBuilder.presentationResponse(instance);
    }
}
