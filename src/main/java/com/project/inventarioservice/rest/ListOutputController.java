package com.project.inventarioservice.rest;

import com.project.inventarioservice.command.ListOutputCmd;
import com.project.inventarioservice.constants.Constants;
import com.project.inventarioservice.response.OutputResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Mariela Cladera M.
 */
@RequestScope
@RestController
public class ListOutputController {

    private ListOutputCmd listOutputCmd;

    public ListOutputController(ListOutputCmd listOutputCmd) {
        this.listOutputCmd = listOutputCmd;
    }

    @GetMapping(value = Constants.OUTPUTS.OUTPUTS_PATH)
    public ResponseEntity<List<OutputResponse>> listOutputs() {
        listOutputCmd.execute();
        return new ResponseEntity<>(listOutputCmd.getInputResponse(), HttpStatus.OK);
    }
}
